/*

status-monitor: system uptime monitoring
Copyright (C) 2016  Alex Moon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

//initate count for page loads since up
var i int

func redirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r,
          	"https://" + Config.Listener.Ip + ":" + Config.Listener.Tlsport + r.URL.String(),
	          http.StatusMovedPermanently)

					fmt.Printf("HTTP Redirect IP: " + Config.Listener.Ip)
					fmt.Printf("HTTP Redirect Port: " + Config.Listener.Tlsport)

}

func Index(w http.ResponseWriter, r *http.Request) {

	//set http headers
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	//Write to html
	fmt.Fprint(w, "Welcome!\n\n")

	//Get Variables
	test := r.URL.Query().Get("test")

	//if test var is not nul write out to html
	if len(test) != 0 {

		fmt.Fprint(w, test)
		//io.WriteString(w, string)  // or
		//w.Write([]byte(string))
	}
}

func getHttp(w http.ResponseWriter, r *http.Request) {

	//http.Redirect(w, url.QueryEscape)

	//set http headers
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	//get Variables
	site := r.URL.Query().Get("site")

	resp, err := HttpHead(site)
	if err != nil {
		fmt.Fprint(w, err)
	}

	//Return JSON to HTML
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		fmt.Fprint(w, "Site or Service not Available.")
	}

	//incrament count
	//i++

	//Print count
	//fmt.Fprint(w, "Count: ", i, "\n")

	//fmt.Fprint(w, r.URL.Query(), "\n")

	//fmt.Fprint(w,  url.QueryEscape(r.URL.Query().Get(*)))

	//fmt.Println(resp.StatusCode)
}

func getKeyword(w http.ResponseWriter, r *http.Request) {

	//http.Redirect(w, url.QueryEscape)

	//set http headers
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	//get Variables
	site := r.URL.Query().Get("site")
	keyword := r.URL.Query().Get("keyword")

	resp, err := HttpSearch(site, keyword)
	if err != nil {
		fmt.Fprint(w, err)
	}

	//Return JSON to HTML
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		fmt.Fprint(w, "Site or Service not Available.")
	}

	//incrament count
	//i++

	//Print count
	//fmt.Fprint(w, "Count: ", i, "\n")

	//fmt.Fprint(w, r.URL.Query(), "\n")

	//fmt.Fprint(w,  url.QueryEscape(r.URL.Query().Get(*)))

	//fmt.Println(resp.StatusCode)
}
