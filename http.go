/*

status-monitor: system uptime monitoring
Copyright (C) 2016  Alex Moon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"errors"
	"github.com/asaskevich/govalidator"
	"net/http"
	"net/url"
	"time"
)

type HttpRequest struct {
	Url string
}

type HttpStatus struct {
	Url    string        `json:"url"`
	Status int           `json:"status"`
	Rtime  time.Duration `json:"rtime"`
}

type HttpRequests []HttpRequest

func HttpHead(site string) (httpstatus HttpStatus, err error) {

	//if var is not null check site status
	if len(site) != 0 {

		//validate site var
		validURL := govalidator.IsURL(site)

		//error if invalid
		if validURL == false {
			return httpstatus, errors.New("Invalid URL")
		}

		//record time to measure response time
		start := time.Now()

		//HEAD request URL
		resp, err := http.Head(site)

		//Stop Response time timer
		Rtime := time.Since(start)

		//handle error from HEAD request
		if err != nil {
			return httpstatus, errors.New("Error requesting HEAD")
		}

		//unescape URL before writeout
		usite, err := url.QueryUnescape(site)
		if err != nil {
			return httpstatus, errors.New("Error requesting Unescaping url")
		}

		//assemble JSON response
		httpstatus := HttpStatus{
			Url:    usite,
			Status: resp.StatusCode,
			Rtime:  Rtime,
		}

		return httpstatus, nil
		//else return no var
	} else {

		return httpstatus, errors.New("No Site Requested!\n")
	}

}
