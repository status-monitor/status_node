/*

status-monitor: system uptime monitoring
Copyright (C) 2016  Alex Moon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"errors"
	"github.com/asaskevich/govalidator"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"time"
)

type HttpKeyword struct {
	Url     string        `json:"url"`
	Keyword string        `json:"keyword"`
	Found   bool          `json:"found"`
	Status  int           `json:"status"`
	Rtime   time.Duration `json:"rtime"`
}

func HttpSearch(site string, keyword string) (httpkeyword HttpKeyword, err error) {

	//if var is not null check site status
	if len(site) != 0 && len(keyword) != 0 {

		//validate site var
		validURL := govalidator.IsURL(site)

		//error if invalid
		if validURL == false {
			return httpkeyword, errors.New("Invalid URL")
		}
        
		//record time to measure response time
		start := time.Now()

		//HEAD request URL
		page, err := http.Get(site)

		//Stop Response time timer
		Rtime := time.Since(start)

		defer page.Body.Close()
		body, err := ioutil.ReadAll(page.Body)

		//fmt.Fprint(w, string(contents))

		//handle error from Get request
		if err != nil {
			return httpkeyword, errors.New("Error requesting Get")
		}

		//Seach for Querys

		found, err := regexp.MatchString(keyword, string(body))
		if err != nil {
			return httpkeyword, errors.New("Error Matctching keyword")
		}

		//unescape URL before writeout
		usite, err := url.QueryUnescape(site)
		if err != nil {
			return httpkeyword, errors.New("Error requesting Unescaping url")
		}

		//assemble JSON response
		httpkeyword := HttpKeyword{
			Url:     usite,
			Keyword: keyword,
			Found:   found,
			Status:  page.StatusCode,
			Rtime:   Rtime,
		}

		return httpkeyword, nil
		//else return no var
	} else {

		return httpkeyword, errors.New("No Site Requested!\n")
	}

}
