/*

status-monitor: system uptime monitoring
Copyright (C) 2016  Alex Moon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package main

import (
	"log"
	"net/http"
	"gitlab.com/status-monitor/config"
)

var Config config.TomlConfig

func main() {

	router := NewRouter()

	Config, err := config.Get()
	if err != nil {
		panic(err)
	}

	log.Print("Starting HTTPS listener...\n")
	log.Print("Listening on addr: " + Config.Listener.Ip)
	log.Print("Listening on port: " + Config.Listener.Tlsport)

	err = http.ListenAndServeTLS(Config.Listener.Ip + ":" + Config.Listener.Tlsport , Config.Listener.Publickeypath , Config.Listener.Privatekeypath , router)
	if err != nil {
		log.Fatal(err)
	}
}
